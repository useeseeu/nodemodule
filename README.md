**Node js 常用模組介紹**

- http模組 ：[http官方文件](https://nodejs.org/api/http.html)
            用來用來建立http server 的一些類別、方法和事件

```
var http = require('http');
var server = http.createServer(function(req, res){
     // do something 
});
 
server.listen(3000);
```
-fs 模組 ：用來傳遞檔案以及對檔案的讀檔開檔寫黨


