var net =require('net');

var server=net.createServer(function(socket){
    var address = server.address();
    var msg='the server is '+JSON.stringify(address);

    //設定最多連接數
    server.maxConnections=3;
    server.getConnections(function(err,cnt){
        console.log('the connection number is'+cnt);
    })

    //send data
    socket.write(msg,function(){
        var writesize=socket.bytesWritten;
        console.log(msg+'sent !');
        console.log('the size of msg',writesize);
    })
    //監聽data
    socket.on('data',function(data){
        console.log(data.toString());
        var readsize=socket.bytesRead;
        console.log('the size of data is'+readsize);
    })
})

server.listen(8080,function(){
    console.log('create server on http://127.0.0.1:8080/')
})