//建立api讀取使用者資訊
var express=require('express');
var app=express();
var fs=require("fs");

app.get('/getDetail/:id',function(req,res){
    //讀取已存在的人類
    fs.readFile(__dirname+"/"+"user.json","utf-8",function(err,data){
        data=JSON.parse(data);
        var user=data["user"+req.params.id]
        console.log(user);
        res.end(JSON.stringify(user));
    });
})

var server =app.listen(3000,function(){
    
    var host=server.address().address
    var port=server.address().port

    console.log('visiting web: http://%s:%s',host,port)
})