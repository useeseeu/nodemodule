//建立用戶端

var net=require('net');

var client=net.Socket();

client.connect(8080,'127.0.0.1',function(){
    console.log('connect to server');
    client.write("msg from client"); //向server端發送資料
})
//監聽server傳來的資料
client.on("data",function(data){
    console.log("the data from server is "+data.toString());

})
//監聽end

client.on("end",function(){
    console.log("end");
})
