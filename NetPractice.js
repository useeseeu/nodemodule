//透過net.Server來建立TCP伺服器 server端

var net=require('net');

//產生實體物件
var server=new net.Server();

//監聽connetion事件
server.on("connection",function(socket){
    console.log('one connection ');
})

//server.listen(3000);
//設定監聽時的function
server.on("listening",function(){
    var address =server.address(); //獲得位址資訊會是一個json檔
    console.log("the port is "+address.port);
    console.log("the address is"+address.address);
    console.log('the family is '+ address.family);
    //檢視ipv4 or 6
})
//設定關閉時的function
server.on('close',function(){
    console.log("server closed!!!");
})
//設定錯誤時的function
server.on('error',function(){
    console.log('error occur');
})

server.listen(3000,function(){
    console.log('create server on http://127.0.0.1:3000/')
})