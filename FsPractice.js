var fs=require('fs')
//開啟檔案
//r+ = 以讀寫模式開啟檔案，如果檔案不存在就拋出例外
console.log('ready to open file');
fs.open('sample.txt','r+',function(err,data){
    if(err){
        console.log(err);
    }
    else{
        console.log('open success')
    }
})
//讀取檔案

fs.readFile('sample.txt','utf-8',function(err,data){
    if(err){
        console.log(err);
    }
    else{
        console.log('read filed :'+data.toString());
    }
})
//寫入資料

var data ="我的錢包就像個洋蔥，每次打開都叫我淚流滿面"
fs.writeFile('sample.txt',data,function(err){
    if(err){
        console.log(err);
    }
    else{
        console.log('written ok ');
    }
})
//取得檔案資訊
fs.stat('sample.txt',function(err,data){
    if (err){
        console.log(err);
    }
    else{
        //確認是否有檔案
        console.log('isfile:'+data.isFile());
        console.log('isDirectory'+data.isDirectory());
        if (data.isFile()){
            console.log('size'+data.size);
            console.log('birthtime'+data.birthtime);
            console.log('mode time'+data.mtime);
        }
    }
})