var express=require('express');
var app=express();
var fs=require("fs");
var user={
    "user4":{
        "name":"Ashely",
        "password":"pass+word",
        "profession":"engineer",
        "id":"4141"    
    }
}

app.get('/addusers',function(req,res){
    //讀取已存在的人類
    fs.readFile(__dirname+"/"+"user.json","utf-8",function(err,data){
         //JSON.parse=>將json轉換為js物件
        data=JSON.parse(data);
        data['user4']=user["user4"];
        console.log(data);
        //JSON.stringify=>將js物件轉換json
        res.end(JSON.stringify(data));
    });
})

var server =app.listen(3000,function(){
    
    var host=server.address().address
    var port=server.address().port

    console.log('visiting web: http://%s:%s',host,port)
})